<?php

namespace Tests\Feature;

use App\Answer;
use App\Question;
use Tests\TestCase;

class QuestionAnswerTest extends TestCase
{
    /**
     * Models tester.
     *
     * @return void
     */
    public function testQuestionAnswerModels()
    {

        $question = new Question();
        $question->text = 'test_question_text';
        $question->save();
        $answer = new Answer();
        $answer->text = 'text_answer_text';
        $answer->question_id = $question->id;
        $answer->save();
        $this->assertEquals($question->id, $question->answer->question_id);

    }




}
