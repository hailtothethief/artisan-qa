<?php

namespace App\Console\Commands;

use App\Question;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class QAndAreset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:reset {db?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Q And A system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Delete all db data or reset questions status.
     *
     */
    public function handle()
    {
        if ($this->confirm('Are you sure?')) {

            try {
                $dbArgument = $this->argument('db');

                if(!is_null($dbArgument)){
                    $this->output->progressStart(Question::all()->count()*2);
                    $exitCode = Artisan::call('migrate:refresh');

                    for ($i = 0; $i < 10; $i++) {
                        sleep(1);
                        $this->output->progressAdvance();
                    }

                    $this->output->progressFinish();
                }else{
                    Question::where('check', '=', 1)->update(['check' => 0]);
                }


                $this->comment("The operation completed successfully". PHP_EOL );

            }
            catch (\Exception $e) {
                throw $e;
            }



        }else{
            $this->comment("Bye!". PHP_EOL );
            return ;
        }

    }

}
