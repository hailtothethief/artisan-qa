<?php

namespace App\Console\Commands;

use App\Answer;
use App\Question;
use Illuminate\Console\Command;


class QAndA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qanda:interactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs an interactive command line based Q And A system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Welcome to the interactive Q&A system');

        $this->mainMenu();

    }

    private function mainMenu() {

        $mainChoices = ['Add question & answer', 'Practice with questions', 'Exit console'];

        $mainChoice = $this->choice(
            'What do you want to do?',
            $mainChoices
        );

        $this->comment("You have chosen: $mainChoice");

        switch ($mainChoice) {
            case "Add question & answer": $this->add();
                break;
            case "Practice with questions": $this->view();
                break;
            case "Exit console":
                $this->out();
                break;
            default:
                $this->add();
                break;
        }

        $this->mainMenu();

    }

    /**
     * Add question and answer.
     *
     */
    private function add() {


        do{
            $questionText = $this->ask('Type the question text (it cannot be empty)');
        }
        while (empty($questionText));

        do{
            $answerText = $this->ask('Type the answer text (it cannot be empty)');
        }
        while (empty($answerText));

        try {

            $question = Question::create([
                'text' => $questionText,
            ]);

            $answer = Answer::create([
                'text' => $answerText,
            ]);
            $question->answer()->save($answer);
            $this->info("Done! ". PHP_EOL);
        }
        catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Show stored questions
     *
     */
    private function view() {

        $questions = Question::all();

        $questionsChoices = [];
        $count = 0;

        if($questions->count() > 0){
            foreach ($questions as $question) {
                array_push($questionsChoices, $question->text);
                if($question->check)
                    $count++;
            }
        }else{
            $this->error("There are no data, you have to add at least one record!");
            return ;
        }

        array_push($questionsChoices, '** Back **', '** Exit console **');

        $choiceMenu = ($count == count($questions)) ? "** ALERT ** You have completed all of your questions! Go back to the previous menu and add a new one" : "Choose which question to practice or go back to the previous step";

        $mainChoice = $this->choice(
            $choiceMenu,
            $questionsChoices
        );

        if($mainChoice == $questionsChoices[count($questionsChoices) - 2]){
            return ;
        }else if($mainChoice == $questionsChoices[count($questionsChoices) - 1]){
            $this->out();
        }

        $chosenQuestion = Question::where('text',$mainChoice)->first();

        if(!$chosenQuestion->check)
            $this->practice($chosenQuestion);
        else
            $this->comment("You've already practiced with this question. Choose another one from the list". PHP_EOL );

        $this->view();


    }


    /**
     * Test user ability
     *
     */
    private function practice($question) {

        do{
            $answerText = $this->ask($question->text. ' (it cannot be empty)');
        }
        while (empty($answerText));

        $dbAnswer = Answer::with('question')->where('question_id', $question->id)->first();

        if( \Illuminate\Support\Str::lower($answerText) == \Illuminate\Support\Str::lower($dbAnswer->text) ){
            try {
                $this->info("Right answer!");
                Question::find($question->id)->update(['check' => 1]);
            }
            catch (\Exception $e) {
                throw $e;
            }

        } else {
            $this->error("Wrong answer");
        }

        $this->results();

    }

    /**
     * Show user results
     *
     */
    private function results() {

        $questions = Question::all(['text', 'check'])->toArray();

        $count = 0;

        foreach ($questions as $question) {
            if($question['check'])
                $count++;
        }

        $headers = ['Question', 'Score'];

        $this->table($headers, $questions);
        $total = count($questions);
        if($count == $total)
            $this->info("Well done! Your practice is over! Your final score is: $count/$total". PHP_EOL );
        else
            $this->comment("Your total score is: $count/$total". PHP_EOL );
    }

    /**
     * Stop interactive console
     *
     */
    private function out() {
        $this->comment("See you next time!". PHP_EOL );
        exit();
    }






}
