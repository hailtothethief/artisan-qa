<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = ['text', 'check'];


    /**
     * Get the answer associated with the questions.
     */
    public function answer()
    {
        return $this->hasOne('App\Answer');
    }

}
