<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';
    protected $fillable = ['text', 'question_id'];

    /**
     * Get the question associated with the answers.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }


}
