<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        $questions = [
            "Who is the president of Canada?",
            "Who hosted the summer olympics in 2004?",
            "What is the most used letter in the english language?",
            "What is the capital of Australia?",
            "Where would you find the Sea of Tranquility?"
        ];

        foreach (range(1,5) as $index) {
            DB::table('questions')->insert([
                'text' => $questions[$index-1],
                'check' => 0,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now')
            ]);

        }

    }
}
