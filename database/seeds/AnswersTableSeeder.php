<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $answers = [
            "Trudeau",
            "Greece",
            "A",
            "Canberra",
            "Moon"
        ];

        foreach (range(1,5) as $index) {
            DB::table('answers')->insert([
                'text' => $answers[$index-1],
                'question_id' => $index,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }
}
